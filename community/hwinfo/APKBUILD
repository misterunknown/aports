# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=hwinfo
pkgver=21.83
pkgrel=0
pkgdesc="Hardware information tool"
url="https://github.com/openSUSE/hwinfo"
arch="all"
license="GPL-2.0-or-later"
makedepends="$depends_dev flex perl libx86emu-dev linux-headers util-linux-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs"
source="$pkgname-$pkgver.tar.gz::https://github.com/openSUSE/hwinfo/archive/$pkgver.tar.gz
	respect-flags.patch
	eudev-udevadm-path.patch"
options="!check"  # no tests provided

export HWINFO_VERSION=$pkgver

build() {
	# Build full and tiny static libraries.
	#make tinystatic  # FIXME: fails
	make static
	cp src/libhd*.a .
	make clean

	# Build full shared library.
	make shared LIBDIR=/usr/lib
}

package() {
	make install \
		LIBDIR=/usr/lib \
		DESTDIR="$pkgdir"

	rmdir "$pkgdir"/sbin "$pkgdir"/usr/lib || true

	install -m 644 -t "$pkgdir"/usr/lib/ libhd.a
	#install -m 644 -t "$pkgdir"/usr/lib/ libhd_tiny.a  # FIXME: disabled above

	mkdir -p "$pkgdir"/usr/share/man/man1
	cp doc/*.1 "$pkgdir"/usr/share/man/man1/

	mkdir -p "$pkgdir"/usr/share/man/man8
	cp doc/*.8 "$pkgdir"/usr/share/man/man8/
}

sha512sums="
58029523bc053d9075256714f88fc2b702f4c3af77ae12dd356cf8c07e2f71d85db0ece4b7ce7aaa02879cf3a8b02837cbb8b47ec6ce10f27755ee0c3d375eec  hwinfo-21.83.tar.gz
581db9d2324c8ba686a1676c9f673885bc927c160d420739b421596c5a8106e80cb26698e2fca8c77147b62667ec4c2a933e6373ad980658048ed1718d906707  respect-flags.patch
3bf22dc8afecbdf080b882358e866dc01bc8837522cf0f47409c514892a0fe0900d060f87d28dd3cb5e0ee4224cbec415273024efd70fcdb947ca004915d2543  eudev-udevadm-path.patch
"
