# Contributor: Simon Zeni <simon@bl4ckb0ne.ca>
# Maintainer: Simon Zeni <simon@bl4ckb0ne.ca>
pkgname=vulkan-validation-layers
_pkgname=Vulkan-ValidationLayers
pkgver=1.3.224
pkgrel=0
pkgdesc="Vulkan Validation Layers"
url="https://www.khronos.org/vulkan/"
arch="all"
license="Apache-2.0"
makedepends="
	cmake
	libx11-dev
	libxcb-dev
	libxrandr-dev
	ninja
	python3
	robin-hood-hashing
	spirv-headers
	spirv-tools-dev
	vulkan-headers
	wayland-dev
	"
subpackages="$pkgname-dbg $pkgname-static $pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/KhronosGroup/Vulkan-ValidationLayers/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"
options="!check" # test segfaults

build() {
	CFLAGS="$CFLAGS -g1" CXXFLAGS="$CXXFLAGS -g1" \
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_DATAROOTDIR=/usr/share \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DBUILD_LAYER_SUPPORT_FILES=ON \
		-DBUILD_WSI_XCB_SUPPORT=ON \
		-DBUILD_WSI_XLIB_SUPPORT=ON \
		-DBUILD_WSI_WAYLAND_SUPPORT=ON \
		-DBUILD_WERROR=OFF

	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7d5891275fb372009175170cba115f93c1645a55b6d27afce693a4909862d31f6e657cd6bf4dd1687dbe219f30754eaa55bd81594d655f09004d847919c1f344  vulkan-validation-layers-1.3.224.tar.gz
"
